<h2>Posts do blog</h2>
<br>
<?php if ($posts): ?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Título</th>
			<th>Slug</th>
			<th>Resumo</th>
			<th>Usuário</th>
			<th>Gerenciar</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($posts as $item): ?>		<tr>

			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->slug; ?></td>
			<td><?php echo $item->summary; ?></td>
			<td><?php echo $item->user->username; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/posts/view/'.$item->id, '<i class ="glyphicon glyphicon-eye-open"></i
					> Visualizar', array('class' => 'btn btn-xs btn-primary')); ?>
					<?php echo Html::anchor('admin/posts/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-xs btn-warning')); ?>
					<?php echo Html::anchor('admin/posts/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-xs btn-danger','onclick' => "return confirm('Are you sure?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Posts.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/posts/create', '<i class="glyphicon glyphicon-plus-sign"></i> Adicionar novo post', array('class' => 'btn btn-success')); ?>

</p>
