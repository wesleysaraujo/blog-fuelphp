<h2>Visualizando #<?php echo $post->id; ?></h2>

<p>
	<strong>Título:</strong>
	<?php echo $post->title; ?></p>
<p>
	<strong>Slug:</strong>
	<?php echo $post->slug; ?></p>
<p>
	<strong>Resumo:</strong>
	<?php echo $post->summary; ?></p>
<p>
	<strong>Conteúdo:</strong>
	<?php echo $post->body; ?></p>
<p>
	<strong>Autor:</strong>
	<?php echo $post->user->username; ?></p>
<div class="btn-group">
	<?php echo Html::anchor('admin/posts/edit/'.$post->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?> 
	<?php echo Html::anchor('admin/posts', '<i class="glyphicon glyphicon-share-alt"></i> voltar', array('class' => 'btn btn-info btn-sm')); ?>
</div>	