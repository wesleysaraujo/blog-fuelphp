<h2>Editando Post</h2>
<br>

<?php echo render('admin/posts/_form'); ?>

	<div class="btn-group">
		<?php echo Html::anchor('admin/posts/view/'.$post->id, '<i class="glyphicon glyphicon-eye-open"></i> Ver', array('class' => 'btn btn-warning btn-sm')); ?>
		<?php echo Html::anchor('admin/posts', '<i class="glyphicon glyphicon-share-alt"></i> Voltar', array('class' => 'btn btn-info btn-sm')); ?>
	</div>
	