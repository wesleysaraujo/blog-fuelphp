<div class="container">
	<h2><?php echo $post->title; ?></h2>
	<small><strong>Postado em: </strong> <?php echo date('d F, Y', $post->created_at); ?> (<?php echo Date::time_ago($post->created_at); ?>), <strong>Por: </strong><?php echo $post->user->username; ?></small>
	<p><?php echo nl2br($post->body); ?></p>
</div>
<hr>
	<div class="panel-group" id="accordion">
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h4 class="panel-title text-info">
	        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
	          Deixe um comentário
	        </a>
	      </h4>
	    </div>
	    <div id="collapseOne" class="panel-collapse collapse out">
	      <div class="panel-body">
	      	<div class="row">
				<div class="col-md-8">
					<?php echo Form::open('blog/comment/'.$post->slug) ?>
						<div class="form-group">
							<?php echo Form::label('Nome', 'name', array('class'=>'control-label')); ?>

								<?php echo Form::input('name', Input::post('name', isset($comment) ? $comment->name : ''), array('class' => 'col-md-3 form-control', 'placeholder'=>'Nome')); ?>

						</div>
						<div class="form-group">
							<?php echo Form::label('E-mail', 'email', array('class'=>'control-label')); ?>

								<?php echo Form::input('email', Input::post('email', isset($comment) ? $comment->email : ''), array('class' => 'col-md-3 form-control', 'placeholder'=>'E-mail')); ?>

						</div>
						<div class="form-group">
							<?php echo Form::label('Website', 'website', array('class'=>'control-label')); ?>

								<?php echo Form::input('website', Input::post('website', isset($comment) ? $comment->website : ''), array('class' => 'col-md-3 form-control', 'placeholder'=>'Website')); ?>

						</div>
						<div class="form-group">
							<?php echo Form::label('Comentário', 'message', array('class'=>'control-label')); ?>

								<?php echo Form::textarea('message', Input::post('message', isset($comment) ? $comment->message : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Comentário')); ?>

						</div>
						<div class="form-group">
							<?php echo Form::hidden('post_id', $post->id, array('class' => 'col-md-3 form-control', 'placeholder'=>'Post id')); ?>
						</div>
						<div class="form-group">
							<label class='control-label'>&nbsp;</label>
							<br/>
							<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary', 'data-loading-text'=> 'Carregando...')); ?>		</div>
					</fieldset>
					<?php echo Form::close() ?>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>
	
<hr>
<h3>Comentários</h3>
<?php foreach($post->comments as $comment): ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo Html::anchor($comment->website, $comment->name); ?> <small>(<?php echo date('d \\d\e\ F, Y', $comment->created_at); ?> <i><?php echo Date::time_ago($comment->created_at); ?></i>)</small>
	</div>
	<div class="panel-body">
		<?php echo $comment->message; ?>
	</div>
</div>
<?php endforeach; ?>