<div class="container">
	<h2>Últimos Posts</h2>

	<?php foreach ($posts as $post):?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo Html::anchor('post/'.$post->slug, $post->title); ?>
			</div>
			<div class="panel-body">
				<p><?php echo $post->summary; ?></p>
			</div>
			<div class="panel-footer">
				<small><strong> Publicado em: </strong><?php echo date('d F, Y', $post->created_at)." (".Date::time_ago($post->created_at).")"; ?> | <strong>Autor: </strong><?php echo $post->user->username;?></small>
			</div>
		</div>
	<?php endforeach; ?>
</div>