<?php echo Form::open(array('class' => 'form-horizontal')) ?>
		<div class="form-group">
			<?php echo Form::label('Username', 'username', array('class' => 'control-label'));?>
			<?php echo Form::input('username',Input::post('username', isset($user) ? $user->username : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Informe o username')) ?>
		</div>

		<div class="form-group">
			<?php echo Form::label('E-mail', 'email', array('class' => 'control-label'));?>
			<?php echo Form::input('email',Input::post('email', isset($user) ? $user->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Informe seu e-mail')) ?>
		</div>

		<div class="form-group">
			<?php echo Form::label('Senha', 'password', array('class' => 'control-label'));?>
			<?php echo Form::password('password',Input::post('password'), array('class' => 'col-md-4 form-control', 'placeholder'=>'Informe sua senha')) ?>
		</div>

		<div class="form-group">
			<?php echo Form::label('Repita a Senha', 'password', array('class' => 'control-label'));?>
			<?php echo Form::password('confirm-password',Input::post('confirme-password'), array('class' => 'col-md-4 form-control', 'placeholder'=>'Repita sua senha')) ?>
		</div>

		<div class="form-group">
			<?php echo Form::label('Perfil', 'profile_fields', array('class' => 'control-label'));?>
			<?php echo Form::textarea('profile_fields', Input::post('profile_fields'), array('class' => 'col-md-4 form-control', 'placeholder'=>'Preencha se perfil', 'rows' => 10)) ?>
		</div>
		
		<?php echo Form::submit('submit', 'Salvar', array('class'=> 'btn btn-primary')) ?>
		
<?php echo Form::close() ?>