<?php
return array(
	'_root_'  => 'blog/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route
	
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
	'post/(:any)'	 => 'blog/view/$1',
	'cadastrar-se'	 => 'user/sign_up',
);