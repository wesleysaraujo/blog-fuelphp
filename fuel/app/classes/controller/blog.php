<?php
class Controller_Blog extends Controller_Base
{
	public function action_index()
	{
		$view = View::forge('blog/index');

		$view->posts = Model_Post::find('all');

		$this->template->title = 'Meu Blog Fuel';
		$this->template->content = $view;
	}

	public function action_view($slug)
	{
		$post = Model_Post::find_by_slug($slug, array('related' => array('user', 'comments')));

		//$this->template->title  = $post->title;
		$this->template->content= View::forge('blog/view', array(
			'post' => $post,
		));
	}

	public function action_comment($slug)
	{
		$post = Model_Post::find_by_slug($slug);

		//Lazy Validation
		if(Input::post('name') AND Input::post('email') AND Input::post('message'))
		{
			//Cria um novo comentário
			$post->comments[] = new Model_Comment(array(
				'name'		=>	Input::post('name'),
				'website'	=>	Input::post('website'),
				'email'		=> 	Input::post('email'),
				'message'	=>	Input::post('message'),
				'user_id' 	=> 	$this->current_user->id,
			));

			//Salva o post e o comentário
			if($post->save())
			{
				$comment = end($post->comments);
				Session::set_flash('success', 'Adicionado comentário #'.$comment->id.'.');
			}
			else
			{
				Session::set_flash('error', 'Não foi possível salvar o comentário');
			}

			Response::redirect('blog/view/'.$slug);
		}
		//Caso os campos não estejam preenchidos
		else
		{
			//Mostra a view apontando os erros
			$this->action_view($slug);
		}
	}
}