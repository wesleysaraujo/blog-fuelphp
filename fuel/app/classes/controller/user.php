<?php
class Controller_User extends Controller_Base
{
	public function action_index()
	{
		$view = View::forge('user/index');

		$this->template->title 	 = "Perfil do usuário";
		$this->template->content = $view; 
	}

	public function action_sign_up()
	{
		$view = View::forge('user/create');

		$this->template->title = "Cadastrar-se como usuário";
		$this->template->content = $view;
	}
}