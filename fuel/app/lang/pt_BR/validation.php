<?php

return array(
	'required'        => 'O campo :label é orbigatório e precisa ter um valor.',
	'min_length'      => 'O campo :label has to contain at least :param:1 characters.',
	'max_length'      => 'O campo :label may not contain more than :param:1 characters.',
	'exact_length'    => 'O campo :label must contain exactly :param:1 characters.',
	'match_value'     => 'O campo :label must contain the value :param:1.',
	'match_pattern'   => 'O campo :label must match the pattern :param:1.',
	'match_field'     => 'O campo :label must match the field :param:1.',
	'valid_email'     => 'O campo :label must contain a valid email address.',
	'valid_emails'    => 'O campo :label must contain a list of valid email addresses.',
	'valid_url'       => 'O campo :label must contain a valid URL.',
	'valid_ip'        => 'O campo :label must contain a valid IP address.',
	'numeric_min'     => 'The minimum numeric value of :label must be :param:1',
	'numeric_max'     => 'The maximum numeric value of :label must be :param:1',
	'numeric_between' => 'O campo :label must contain a numeric value between :param:1 and :param:2',
	'valid_string'    => 'O campo string rule :rule(:param:1) failed for field :label',
	'required_with'   => 'O campo :label must contain a value if :param:1 contains a value.',
	'valid_date'      => 'O campo :label must contain a valid formatted date.',
);
